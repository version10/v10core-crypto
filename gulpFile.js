// Class Name
var className = "Crypto";

// Import
var gulp = require('gulp');
var concat = require('gulp-concat');
var browserify = require("browserify");
var babelify = require("babelify");
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

function scripts() {
    return browserify({
        entries: './src/js/'+className+'.js',
        debug: false,
        transform: [babelify.configure({
            presets: ['@babel/preset-env'],
            compact: false
        })]
    })
        .bundle()
        .pipe(source('./src/js/'+className+'.js'))
        .pipe(buffer())
        .pipe(concat(className+'.min.js'))
        .pipe(gulp.dest('./dist/'));
}

var build = gulp.series(gulp.parallel(scripts));

exports.scripts = scripts;

exports.default = build;