let CryptoJS = require("crypto-js");
let cryptoAES = require("crypto-js/aes");


export default class AES
{
  constructor(){}

  static decrypt (value, passphrase)
  {
    let obj_json = JSON.parse(value);
    let encrypted = obj_json.ciphertext;
    let salt = CryptoJS.enc.Hex.parse(obj_json.salt);
    let iv = CryptoJS.enc.Hex.parse(obj_json.iv);
    let key = CryptoJS.PBKDF2(passphrase, salt, {hasher: CryptoJS.algo.SHA512, keySize: 64 / 8, iterations: 999});
    let decrypted = CryptoJS.AES.decrypt(encrypted, key, {iv: iv});

    return decrypted.toString(CryptoJS.enc.Utf8)
  }

  static encrypt(passphrase, plain_text)
  {
    var salt = CryptoJS.lib.WordArray.random(256);
    var iv = CryptoJS.lib.WordArray.random(16);
    var key = CryptoJS.PBKDF2(passphrase, salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999 });
    var encrypted = CryptoJS.AES.encrypt(plain_text, key, {iv: iv});

    var data = {
      ciphertext : CryptoJS.enc.Base64.stringify(encrypted.ciphertext),
      salt : CryptoJS.enc.Hex.stringify(salt),
      iv : CryptoJS.enc.Hex.stringify(iv)
    }

    return JSON.stringify(data);
  }

}

